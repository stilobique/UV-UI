import bpy


# -----------------------------------------------------------------------------
# Substance Project panel
# -----------------------------------------------------------------------------
class Operator(bpy.types.Panel):
    bl_label = "Transform"
    bl_space_type = "IMAGE_EDITOR"
    bl_region_type = "TOOLS"
    bl_category = "Tools"

    def draw(self, context):
        layout = self.layout

        layout.label("Rotation")
        icon = "META_EMPTY"

        layout.label("Offset")
        icon = "META_PLANE"


def register():
    bpy.utils.register_class(Operator)


def unregister():
    bpy.utils.unregister_class(Operator)

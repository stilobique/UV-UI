# -----------------------------------------------------------------------------
# Import all Package addon
# -----------------------------------------------------------------------------
import sys
import importlib

modulesNames = [
    # Models
    # 'models.project',
    # views
    'views.views-transform',
    # Controllers
    # 'controllers.SubstancePainter',
    ]

modulesFullNames = []
for currentModule in modulesNames:
    modulesFullNames.append('{}.{}'.format(__name__, currentModule))

for currentModule in modulesFullNames:
    if currentModule in sys.modules:
        importlib.reload(sys.modules[currentModule])
    else:
        globals()[currentModule] = importlib.import_module(currentModule)


# -----------------------------------------------------------------------------
# MetaData Add-On Blender
# -----------------------------------------------------------------------------
bl_info = {
    "name": "UV UI",
    "author": "stilobique",
    "version": (0, 1, 0),
    "blender": (2, 78),
    "location": "UV/Image Editor",
    "description": "Add an UI to various UI Tools.",
    "warning": "",
    "category": "UV",
    "wiki_url": "https://github.com/stilobique/UV-UI/wiki",
    "tracker_url": "https://github.com/stilobique/UV-UI/issues",
}


# -----------------------------------------------------------------------------
# Update register all methods to this addons
# -----------------------------------------------------------------------------
def register():
    # Add all class present in this addon
    for currentModule in modulesFullNames:
        if currentModule in sys.modules:
            if hasattr(sys.modules[currentModule], 'register'):
                sys.modules[currentModule].register()


# -----------------------------------------------------------------------------
# Delete register
# -----------------------------------------------------------------------------
def unregister():
    for currentModule in modulesFullNames:
        if currentModule in sys.modules:
            if hasattr(sys.modules[currentModule], 'unregister'):
                sys.modules[currentModule].unregister()
